import 'package:flutter/material.dart';
import 'package:passing_data_value/model/user_model.dart';
import 'page_get_data.dart';


class PagePassingValue extends StatefulWidget {
  @override
  _PagePassingValueState createState() => _PagePassingValueState();
}

class _PagePassingValueState extends State<PagePassingValue> {

  // declaring variables for editingcontroller


  var userController = new TextEditingController();
  var emailController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Passing Value Page'),
        backgroundColor: Colors.green,
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget> [
              Padding(
                child: new Text("Please input data ", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                textAlign: TextAlign.center,
                ),
                padding: EdgeInsets.only(bottom: 20.0),
              ),
              TextFormField(
                controller: userController,
                decoration: InputDecoration(
                  hintText: 'Input Username'
                ),
              ),

              TextFormField(
                controller: emailController,
                decoration: InputDecoration(
                  hintText: 'Imput Email'
                ),
              ),
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                onPressed: (){
                  // send data other page
                   Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageGetValue(
                    value: UserModel(
                      username: userController.text,
                      email: emailController.text
                    ),
                  )));

                },child: Text('Submit'),
              )
            ],
          ),
        ),
      ),
    );
  }
}


