# Happy Flutter - Sport News Apps Flutter

## Passing Data Value - Example ##

![Screenshot](images/page01.png)
![Screenshot](images/page02.png)
![Screenshot](images/page03.png)

## Return Value - Example ##

### Mainpage ###
Application main page

![Sreenshot](images/page04.png)

Choose Option page 

![Sreenshot](images/page05.png)

Click yes button navigate main page and show test "this is option 'Yes'" bottom of screen 

![Sreenshot](images/page06.png)

Click no button navigate main page and show test "this is option 'no'" bottom of screen 

![Sreenshot](images/page07.png)

## Sport News App ##

- Design Mysql database and JOSN javascript object notation webservice using PHP

Main page sport news using CARDS

![Sreenshot](images/page08.png)

Detailed news page

![Sreenshot](images/page09.png)