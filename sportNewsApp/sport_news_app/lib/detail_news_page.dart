import 'package:flutter/material.dart';

class PageDetailNews extends StatefulWidget {

  List list;
  int index;

  PageDetailNews({this.list, this.index});
  @override
  _PageDetailNewsState createState() => _PageDetailNewsState();
}

class _PageDetailNewsState extends State<PageDetailNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.list[widget.index]['judul_berita']}"),
        backgroundColor: Colors.deepOrange,
      ),

      body: new ListView(
        children: <Widget> [
          new Image.network('http://192.168.1.251/sport_news_server/images/' + widget.list[widget.index]['gbr_berita']),
          new Container(
            padding: const EdgeInsets.all(32.0),
            child: new Row(
              children: <Widget>[
              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: new Text('Title : ' + widget.list[widget.index]['judul_berita'], style: new TextStyle(
                        fontWeight: FontWeight.bold,
                      ),),
                    ),
                    new Text('Date : ' + widget.list[widget.index]['tgl_berita'], style: new  TextStyle(
                    color: Colors.redAccent
                    ),),

                  ],
                )
          ),
                new Icon(Icons.star, color: Colors.redAccent,)
              ],
          ),
          ),
          new Container(
            padding:  const EdgeInsets.all(32.0),
            child: new Text(widget.list[widget.index]['isi_berita'],softWrap: true,)
          )
        ],
      )
    );
  }
}
