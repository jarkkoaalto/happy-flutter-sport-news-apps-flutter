import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'detail_news_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,

      ),
      home: PageHomeSportNews(),debugShowCheckedModeBanner: false,
    );
  }
}

class PageHomeSportNews extends StatelessWidget {

  // Connecting local xampp mysql database, using php connection
  Future<List> getData() async {
    final responseData = await http.get("http://192.168.1.251/sport_news_server/get_berita.php");
    return json.decode(responseData.body);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sport News'),
        backgroundColor: Colors.redAccent,
      ),
      body: FutureBuilder<List>(
        future: getData(),
        builder: (context, snapshot){
          // check has data or error
          if(snapshot.hasError) print(snapshot.error);

          return snapshot.hasData ? new ItemListSportNews(list: snapshot.data) : new  Center(child: CircularProgressIndicator());
        },
      )
    );
  }
}



class ItemListSportNews extends StatelessWidget {

  final List list;
  ItemListSportNews({this.list});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount:  list == null ? 0: list.length,
      itemBuilder: (context,i){
        return new Container(
          padding: const EdgeInsets.all(10.0),
          child: new GestureDetector(
            onTap: (){

              Navigator.of(context).push(new MaterialPageRoute(builder: (context) => PageDetailNews(
                list: list,
                index: i,
              )));
            },
            child: new Card(
              child: new ListTile(
                title: Text(list[i]['judul_berita'], style: new TextStyle(fontWeight: FontWeight.bold, color:  Colors.deepOrange),),
                subtitle: new Text("Date: ${list[i]['tgl_berita']}"),
                trailing: new Image.network('http://192.168.1.251/sport_news_server/images/' + list[i]['gbr_berita'],
                    fit: BoxFit.cover,
                    height: 60.0,
                    width:60.0),
              ),
            ),
          ),
        );
      },
    );
  }
}

