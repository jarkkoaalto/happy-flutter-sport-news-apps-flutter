import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: PageHomeReturnData(), debugShowCheckedModeBanner: false,
    );
  }
}

class PageHomeReturnData extends StatefulWidget {
  @override
  _PageHomeReturnDataState createState() => _PageHomeReturnDataState();
}

class _PageHomeReturnDataState extends State<PageHomeReturnData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Return Data'),
        backgroundColor: Colors.orangeAccent,

      ),
      body: Center(
        child: SelectionButton(),
      ),
    );
  }
}
class SelectionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return RaisedButton(
      onPressed: (){
        _navigateAndDisplaySelectionItem(context);
      }, child: Text('Please click here to see an option!'),
    );
  }

  _navigateAndDisplaySelectionItem(BuildContext context) async{
    final resultOption = await Navigator.push(context, MaterialPageRoute(builder: (context) => SelectionOptionScreen()));

    Scaffold.of(context)
    // Can cut
    // Scaffold.of(context).removeCurrentSnackBar();
    // Scaffold.of(context).showSnackBar(snackbar);
    ..removeCurrentSnackBar()
    ..showSnackBar(SnackBar(content: Text("$resultOption")));
  }
}

class SelectionOptionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose an Option'),
        backgroundColor: Colors.deepOrange,
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MaterialButton(onPressed: (){
                  Navigator.pop(context, "This is option 'yes'");
              }, color: Colors.deepOrange,
              textColor: Colors.white,
              child: Text('Yes')),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MaterialButton(onPressed: (){
                Navigator.pop(context, "This is option 'no'");
              }, color: Colors.deepOrange,
                  textColor: Colors.white,
                  child: Text('No')),
            )
          ],
        )
      )
    );
  }
}
